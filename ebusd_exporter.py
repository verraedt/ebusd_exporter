#!/usr/bin/python3

from prometheus_client import start_http_server, Gauge
import os
import logging
import socket
import re
import time

logging.basicConfig(level=logging.INFO)

METRICS = {
    'EnvTempIn',
    'EnvTempOut',
    'EnergyIntegral',
    'FlowTempDesired',
    'FlowTempOut',
    'CurrentPowerGenerated',
    'CurrentPowerConsumption',
    'YieldTotal',
#    'ConsumptionTotal',
    'EnergyHc',
    'EnergyCool',
    'Hours',
    'HoursHc',
    'HoursCool',
    'HoursHwc',
    'State',
    'WaterPressure',
    'OutsideTemp',
    'OutsideTempAvg',
    'HwcStorageTemp',
    'Hc1FlowTemp',
    'Hc1Status',
    'Hc1PumpStatus',
    'z1RoomTemp',
    'z1ActualRoomTempDesired',
    'Status01',
    'Status02',
}

def read_metric(host, port, metric):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        s.sendall(("r -v -n %s\n" % metric).encode('ascii'))
        data = s.recv(1024)
        return data.decode('ascii').split(' ')[2]

if __name__ == "__main__":
    # Configuration
    host = os.getenv('EBUSD_HOST', 'ebusd.service.consul')
    port = os.getenv('EBUSD_PORT', 8888)
    refresh_interval = os.getenv('REFRESH_INTERVAL', 58)

    # Collect gauges
    gauges = {}
    labels = ['field', 'unit']
    pattern = re.compile(r'(?<!^)(?=[A-Z])')
    for metric in METRICS:
        name = pattern.sub('_', metric).lower()
        gauges[metric] = Gauge('ebusd_%s' % name, metric, labels)

    # Start prometheus
    start_http_server(9000)

    # Main loop
    while True:
        for metric in METRICS:
            try:
                val = read_metric(host, port, metric).strip()
                print("%s: %s" % (metric, val))

                for index, value in enumerate(val.split(';')):
                    k,v = value.split('=')
                    gauges[metric].labels(index + 1, k).set(float(v))
            except Exception as e:
                print(e)

        time.sleep(refresh_interval)
