FROM python:3

WORKDIR /usr/src/app
RUN pip install --no-cache-dir prometheus_client requests

EXPOSE 9000

COPY ./ebusd_exporter.py /usr/src/app

CMD ["python", "ebusd_exporter.py"]
